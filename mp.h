 *not* raise OSError (even if the wrapped syscall does)
        N)r(   )r   r   r   r   r   �
unregisterl   s    zBaseSelector.unregisterc             C   s    |  j  | � |  j | | | � S)ay  Change a registered file object monitored events or attached data.

        Parameters:
        fileobj -- file object or file descriptor
        events  -- events to monitor (bitwise mask of EVENT_READ|EVENT_WRITE)
        data    -- attached data

        Returns:
        SelectorKey instance

        Raises:
        Anything that unregister() or register() raises
        )r*   r)   )r   r   r   r   r   r   r   �modify   s    zBaseSelector.modifyc             C   s
   t  � d S)aq  Perform the actual selection, until some monitored file objects are
        ready or a timeout expires.

        Parameters:
        timeout -- if timeout > 0, this specifies the maximum wait time, in
                   seconds
                   if timeout <= 0, the select() call won't block, and will
                   report the currently ready file objects
                   if timeout is None, select() will block until a monitored
                   file object becomes ready

        Returns:
        list of (key, events) for ready file objects
        `events` is a bitwise mask of EVENT_READ|EVENT_WRITE
        N)r(   )r   �timeoutr   r   r   �select�   s    zBaseSelector.selectc             C   s   d S)zmClose the selector.

        This must be called to make sure that any underlying resource is freed.
        Nr   )r   r   r   r   �close�   s    zBaseSelector.closec             C   sI   |  j  �  } y | | SWn* t k
 rD t d j | � � d � Yn Xd S)zzReturn the key associated to a registered file object.

        Returns:
        SelectorKey for this file object
        z{!r} is not registeredN)�get_mapr   r   )r   r   �mappingr   r   r   �get_key�   s
    zBaseSelector.get_keyc             C   s
   t  � d S)z2Return a mapping of file objects to selector keys.N)r(   )r   r   r   r   r/   �   s    zBaseSelector.ge
#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

char buf[512];

void
cpt_with_one_arg(int fd)
{
  int read_bytes;
  read_bytes = read(0, buf, sizeof(buf));
  write(fd ,buf,read_bytes);
  close(fd);
}

void
cpt_with_two_args(int src_fd,int dst_fd)
{
  int read_bytes;
  read_bytes = read(src_fd, buf, sizeof(buf));
  close(src_fd);
  write(dst_fd ,buf,read_bytes);
  close(dst_fd);
}

int
main(int argc, char **argv)
{
  switch(argc){
    case 2:
        ;//one arg to copy
        int fd;
        fd = open(argv[1], O_CREATE | O_WRONLY);
        cpt_with_one_arg(fd);
        break;
    case 3:
        ;//two args to copy
        int src_fd,dst_fd;
        src_fd = open(argv[1], O_RDONLY);
        if( src_fd < 0){
            printf(1, "cpt: cannot open %s\n", argv[1]);
            exit();
        }
        dst_fd = open(argv[2], O_CREATE | O_WRONLY);
        cpt_with_two_args(src_fd,dst_fd);
        break;
    default:
        printf(1, "cpt: not match true command\n");
        break;
  }
  exit();
}

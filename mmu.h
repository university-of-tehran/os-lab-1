ct)r#   r$   r%   r&   r   r)   r*   r-   r   r   )rE   r   rU   K  s
   
rU   �epollc                   s|   e  Z d  Z d Z �  f d d �  Z d d �  Z d �  f d d � Z �  f d	 d
 �  Z d d d � Z �  f d d �  Z	 �  S)�EpollSelectorzEpoll-based selector.c                s    t  �  j �  t j �  |  _ d  S)N)rA   r   r-   r]   �_epoll)r   )rE   r   r   r   �  s    zEpollSelector.__init__c             C   s   |  j  j �  S)N)r_   r	   )r   r   r   r   r	   �  s    zEpollSelector.filenoNc                sl   t  �  j | | | � } d } | t @r8 | t j O} n  | t @rR | t j O} n  |  j j | j | � | S)Nr   )	rA   r)   r:   r-   �EPOLLINr;   �EPOLLOUTr_   r   )r   r   r   r   r9   Zepoll_events)rE   r   r   r)   �  s    

zEpollSelector.registerc                sB   t  �  j | � } y |  j j | j � Wn t k
 r= Yn X| S)N)rA   r*   r_   r   �OSError)r   r   r9   )rE   r   r   r*   �  s    zEpollSelector.unregisterc       	      C   s  | d  k r d } n, | d k r* d } n t  j | d � d } t |  j � } g  } y |  j j | | � } Wn t k
 r� | SYn Xx� | D]z \ } } d } | t j @r� | t	 O} n  | t j
 @r� | t O} n  |  j | � } | r� | j | | | j @f � q� q� W| S)Nr   r   g     @�@g����MbP?�����)rY   rZ   r   r   r_   rT   rO   r-   r`   r;   ra   r:   r?   rP   r   )	r   r,   �max_evrQ   r[   r   r\   r   r9   r   r   r   r-   �  s*    			!zEpollSelector.selectc                s   |  j  j �  t �  j �  d  S)N)r_   r.   rA   )r   )rE   r   r   r.   �  s    zEpollSelector.close)
r#   r$   r%   r&   r   r	   r)   r*   r-   r.   r   r   )rE   r   r^   ~  s   

r^   �kqueuec                   s|   e  Z d  Z d Z �  f d d �  Z d d �  Z d �  f d d � Z �  f d	 d
 �  Z d d d � Z �  f d d �  Z	 �  S)�KqueueSelectorzKqueue-based selector.c                s    t  �  j �  t j �  |  _ d  S)N)rA   r   r-   re   �_kqueue)r   )rE   r   r   r   �  s    zKqueueSelector.__init__c             C   s   |  j  j �  S)N)rg   r	   )r   r   r   r   r	   �  s    zKqueueSelector.filenoNc                s�   t  �  j | | | � } | t @r\ t j | j t j t j � } |  j j	 | g d d � n  | t
 @r� t j | j t j t j � } |  j j	 | g d d � n  | S)Nr   )rA   r)   r:   r-   �keventr   �KQ_FILTER_READZ	KQ_EV_ADDrg   �controlr;   �KQ_FILTER_WRITE)r   r   r   r   r9   �kev)rE   r   r   r)   �  s    

zKqueueSelector.registerc                s�   t  �  j | � } | j t @rr t j | j t j t j � } y |  j	 j
 | g d d � Wqr t k
 rn Yqr Xn  | j t @r� t j | j t j t j � } y |  j	 j
 | g d d � Wq� t k
 r� Yq� Xn  | S)Nr   )rA   r*   r   r:   r-   rh   r   ri   ZKQ_EV_DELETErg   rj   rb   r;   rk   )r   r   r9   rl   )rE   r   r   r*   �  s     zKqueueSelector.unregisterc       
      C   s  | d  k r d  n t  | d � } t |  j � } g  } y |  j j d  | | � } Wn t k
 rj | SYn Xx� | D]� } | j } | j } d } | t j	 k r� | t
 O} n  | t j k r� | t O} n  |  j | � }	 |	 rr | j |	 | |	 j @f � qr qr W| S)Nr   )rN   r   r   rg   rj   rO   �ident�filterr-   ri   r:   rk   r;   r?   rP   r   )
r   r,   rd   rQ   Zkev_listrl   r   �flagr   r9   r   r   r   r-   �  s&    !			!zKqueueSelector.selectc                s   |  j  j �  t �  j �  d  S)N)rg   r.   rA   )r   )rE   r   r   r.   �  s    zKqueueSelector.close)
r#   r$   r%   r&   r   r	   r)   r*   r-   r.   r   r   )rE   r   rf   �  s   rf   r   �   )r&   �abcr   r   �collectionsr   r   rY   r-   rR   r:   r;   r   r   r   r'   r6   r@   �hasz_BaseSelectorImpl.unregisterc             C   s�   y |  j  |  j | � } Wn* t k
 rF t d j | � � d  � Yn X| | j k r{ |  j | � |  j | | | � } n4 | | j k r� | j d | � } | |  j  | j	 <n  | S)Nz{!r} is not registeredr   )
r   r   r   r   r   r*   r)   r   �_replacer   )r   r   r   r   r9   r   r   r   r+   �   s    z_BaseSelectorImpl.modifyc             C   s   |  j  j �  d  S)N)r   �clear)r   r   r   r   r.     s    z_BaseSelectorImpl.closec             C   s   |  j  S)N)r7   )r   r   r   r   r/     s    z_BaseSelectorImpl.get_mapc             C   s,   y |  j  | SWn t k
 r' d SYn Xd S)z�Return the key associated to a given file descriptor.

        Parameters:
        fd -- file descriptor

        Returns:
        corresponding key, or None if not found
        N)r   r   )r   r   r   r   r   �_key_from_fd  s    	z_BaseSelectorImpl._key_from_fd)r#   r$   r%   r&   r   r   r)   r*   r+   r.   r/   r?   r   r   r   r   r6   �   s   r6   c                   s�   e  Z d  Z d Z �  f d d �  Z d �  f d d � Z �  f d d �  Z e j d	 k rl d d
 d � Z	 n	 e
 j
 Z	 d d d � Z
 �  S)�SelectSelectorzSelect-based selector.c                s)   t  �  j �  t �  |  _ t �  |  _ d  S)N)�superr   �set�_readers�_writers)r   )�	__class__r   r   r     s    zSelectSelector.__init__Nc                s\   t  �  j | | | � } | t @r8 |  j j | j � n  | t @rX |  j j | j � n  | S)N)rA   r)   r:   rC   �addr   r;   rD   )r   r   r   r   r9   )rE   r   r   r)     s    

zSelectSelector.registerc                s<   t  �  j | � } |  j j | j � |  j j | j � | S)N)rA   r*   rC   �discardr   rD   )r   r   r9   )rE   r   r   r*   &  s    zSelectSelector.unregister�win32c             C   s2   t  j  | | | | � \ } } } | | | g  f S)N)r-   )r   �r�w�_r,   �xr   r   r   �_select-  s    !zSelectSelector._selectc       	      C   s  | d  k r d  n t  | d � } g  } y+ |  j |  j |  j g  | � \ } } } Wn t k
 rj | SYn Xt | � } t | � } x| | | BD]p } d } | | k r� | t O} n  | | k r� | t O} n  |  j | � } | r� | j	 | | | j
 @f � q� q� W| S)Nr   )�maxrM 